WURFL WS provides a wrapper to WURFLWS (http://www.wurflws.com/), a web service
offering WURFL (Wireless Universal Resource FiLe) lookups. Commonly, this is 
used to detect mobile devices.

Whilst the WURFL file - and an API - is freely available, it must be regularly 
updated and takes up a fairly significant amount of disk space. Using the 
webservice (which is free) does not incur this overhead.

WURFL WS also integrates with the Mobile Tools module. 
(http://drupal.org/project/mobile_tools)

You can read more about WURFL on the project homepage. 
(http://wurfl.sourceforge.net/)
 
Required modules

* HTTP Client
* Autoload